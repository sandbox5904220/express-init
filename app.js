require('dotenv').config();
const express = require('express');
const cors = require('cors')
const app = express();
const path = require('path');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const YAML = require('yaml');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const rfs = require('rotating-file-stream');
const RoutePath = require('./assets/routePath');
const db = require('./controller/db');

//? Config
app.use(cors());
app.use(logger('dev', { skip: (req, res) => { return res.statusCode < 400 } }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//? Logs
let pad = (num) => `${num > 9 ? "" : "0"}${num}`;
let time = new Date();
let filename = `${pad(time.getMonth())}-${time.getFullYear()}-log.log`;
const stream = rfs.createStream(filename, { interval: '1M', path: path.join(__dirname, 'logs') });
app.use(logger(
    (tokens, req, res) => {
        return [
            tokens.date(req, res, 'iso'),
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            tokens.res(req, res, 'body'), '-',
            tokens['response-time'](req, res), 'ms'
        ].join(' ');
    },
    {
        skip: (req, res) => { return res.statusCode < 400 },
        stream: stream
    }
));

//? View
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(YAML.parse(fs.readFileSync('./assets/api_doc.yaml', 'utf8'))));
app.use(express.static(path.join(__dirname, 'public')));

//? Routes
const indexRouter = require(RoutePath.INDEX.route);
const demoRouter = require(RoutePath.DEMO.route);

app.use(RoutePath.INDEX.path, indexRouter);
app.use(RoutePath.DEMO.path, demoRouter);


//? Main
app.listen(process.env.PORT, () => {
    console.log(`App listening on ${process.env.HOST_TEST}`);
    db.auth;
    db.sequelize.sync();

})
