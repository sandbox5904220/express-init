const dbPath = {
  isConnect: 'Connection has been established successfully.',
  errConnect: 'Unable to connect to the database:',
  admin: '../models/admin.db',
}

module.exports = dbPath
