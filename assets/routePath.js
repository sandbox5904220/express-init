const RoutePath = {
  INDEX: { route: './routes/index', path: '/' },
  DEMO: { route: './routes/demo', path: '/demo' },
}

module.exports = RoutePath
