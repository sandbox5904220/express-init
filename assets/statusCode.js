const statusCode = {
    _comment: "DE=Device,PG=Page",
    s200: {
        code: 200,
        statusCode: "0000",
        statusMessage: "Success"
    },
    s4011: {
        code: 401,
        statusCode: "PG0004011",
        statusMessage: "Unauthorized"
    },
    s4031: {
        code: 403,
        statusCode: "PG0004031",
        statusMessage: "Forbidden"
    },
    s4041: {
        code: 404,
        statusCode: "PG0004041",
        statusMessage: "Sorry, can't find that"
    },
    s5001: {
        code: 500,
        statusCode: "DE0005001",
        statusMessage: "Internal Server Error"
    },
    s5041: {
        code: 504,
        statusCode: "DE0005041",
        statusMessage: "Mqtt Broker Timeout"
    },
    s5042: {
        code: 504,
        statusCode: "DE0005042",
        statusMessage: "Public Timeout"
    },
    DB1001: {
        code: 1001,
        statusCode: "DB0000001",
        statusMessage: "DeviceID is null"
    },
    DB1002: {
        code: 500,
        statusCode: "DB0000002",
        statusMessage: "Duplicate entry"
    },
    DB1003: {
        code: 500,
        statusCode: "DB0000003",
        statusMessage: "Error add log device version"
    },
    DB1004: {
        code: 500,
        statusCode: "DB0000004",
        statusMessage: "Error add device"
    },
    DB1005: {
        code: 500,
        statusCode: "DB0000005",
        statusMessage: "Error update device"
    },
    DB1006: {
        code: 500,
        statusCode: "DB0000006",
        statusMessage: "Error delete device"
    },
    DB1007: {
        code: 500,
        statusCode: "DB0000007",
        statusMessage: "Error add customer balance"
    }
}

module.exports = statusCode;