module.exports = (sequelize, Sequelize) => {
    const admin = sequelize.define(
        'admin',
        {
            ID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, field: 'ID' },
            userName: { type: Sequelize.TEXT, allowNull: false, unique: true, field: 'userName' },
            passWord: { type: Sequelize.TEXT, allowNull: false, field: 'passWord' },
            status: { type: Sequelize.INTEGER, field: 'status' },
        },
        {
            tableName: 'admin',
            timestamps: false
        }
    );

    return admin;
}