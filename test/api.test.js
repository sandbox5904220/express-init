const supertest = require('supertest');
require('dotenv').config()
const request = supertest.agent(process.env.HOST_TEST);

describe('INDEX', () => {
    describe('Health Check', () => {
        it('w/o auth', () => {
            return request.get('/')
                .expect(200);
        });
        it('auth', () => {
            return request.get('/')
                .set({ 'x-api-key': process.env.XAPIKEY })
                .expect(200);
        });
    });

    describe('Document', () => {
        it('w/o auth', () => {
            return request.get('/api-docs')
                .expect(301);
        });
        it('auth', () => {
            return request.get('/api-docs')
                .set({ 'x-api-key': process.env.XAPIKEY })
                .expect(301);
        });
    });

    describe('Demo', () => {
        it('w/o auth', () => {
            return request.get('/demo')
                .expect(200);
        });
        it('auth', () => {
            return request.get('/demo')
                .set({ 'x-api-key': process.env.XAPIKEY })
                .expect(200);
        });
    });
});

// describe('Dashboard', () => {
//     describe('Admin', () => {
//         it('w/o auth', () => {
//             return request.get('/admin/all')
//                 .expect(401);
//         });
//         it('getAdmin', () => {
//             return request.get('/admin/all')
//                 .set({ 'x-api-key': Env.XApiKey })
//                 .expect(200)
//                 .then(res => {
//                     expect(res.body.statusCode).toEqual("0000");
//                 });
//         });
//         it('addAdmin', () => {
//             return request.post('/admin/add')
//                 .set({ 'x-api-key': Env.XApiKey })
//                 .send({
//                     "username": "test",
//                     "password": "test"
//                 })
//                 .expect(200)
//                 .then(res => {
//                     expect(res.body.statusCode).toEqual("0000");
//                 });
//         });
//         it('updateAdmin', () => {
//             return request.post('/admin/update')
//                 .set({ 'x-api-key': Env.XApiKey })
//                 .send({
//                     "adminID": 0,
//                     "username": "test",
//                     "password": "testtest"
//                 })
//                 .expect(200)
//                 .then(res => {
//                     expect(res.body.statusCode).toEqual("0000");
//                 });
//         });
//         it('delAdmin', () => {
//             return request.get('/admin/del')
//                 .set({ 'x-api-key': Env.XApiKey })
//                 .send({
//                     "adminID": 0,
//                     "username": "test",
//                     "password": "testtest"
//                 })
//                 .expect(200)
//                 .then(res => {
//                     expect(res.body.statusCode).toEqual("0000");
//                 });
//         });
//     })
// });