# Hello!!

Project is a Express framwork template vsersion 1.0.0

## Installation

1. Use the package manager [npm](https://www.npmjs.com/) to install node_modules.

```bash
npm install
```

2. Create ``` .env.developer ``` from ``` .env.developer ```

## Packet use

```json
"dependencies": {
    "cookie-parser": "~1.4.4",
    "cors": "^2.8.5",
    "debug": "~2.6.9",
    "dotenv": "^16.0.3",
    "dotenv-cli": "^7.2.1",
    "express": "~4.16.1",
    "jsonwebtoken": "^9.0.0",
    "morgan": "~1.9.1",
    "rotating-file-stream": "^3.1.0",
    "sequelize": "^6.31.1",
    "sqlite3": "^5.1.6",
    "swagger-ui-express": "^4.6.3",
    "yaml": "^2.3.1"
  },
"devDependencies": {
    "jest": "^29.5.0",
    "supertest": "^6.3.3"
  }
"gobal": {
    "nodemon": "version?"
  }
```

## License

[Ch[A]mp](https://gitlab.com/ChampTH)