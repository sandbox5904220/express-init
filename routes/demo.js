const express = require('express');
const router = express.Router();
const demoController = require('../controller/demoController');
const auth = require('../controller/utility/auth');
const utility = require('../controller/utility/utility');



//? Method
router.get('/', demoController.getAdmin);

router.post('/login', auth.login);

router.post('/test', auth.auth, utility.afterAuth);



module.exports = router;
