const express = require('express');
const router = express.Router();
const text = require('../assets/index');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: text.INDEX });
});

module.exports = router;
