require('dotenv').config();
const jwt = require('jsonwebtoken');
const statusCode = require('../assets/statusCode');
const text = require('../assets/demo');
const db = require('./db');
const { Op, sequelize, admin } = db;

const controller = {};

controller.getAdmin = async (req, res, next) => {
    let Admin = await admin.findAll();
    res.json({ status: statusCode.s200, mytext: text.DEMO, value: JSON.parse(JSON.stringify(Admin)) });
};

module.exports = controller;