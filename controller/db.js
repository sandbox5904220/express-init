require('dotenv').config()
const { Sequelize, Op } = require('sequelize');
const dbPath = require('../assets/dbPath');

//? Passing parameters separately (other dialects)
const sequelize = new Sequelize(process.env.NAME_DB, process.env.USERNAME_DB, process.env.PASSWORD_DB, {
  host: process.env.HOST_DB,
  dialect: 'sqlite',/* one of 'mysql' | 'postgres' | 'sqlite' | 'mariadb' | 'mssql' | 'db2' | 'snowflake' | 'oracle' */
  logging: false,
});

//? connect
const auth = async () =>{
    try {
        await sequelize.authenticate();
        console.log(dbPath.isConnect);
    } catch (error) {
        console.error(db.errConnect, error);
    }
}

//? init
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Op = Op;
db.auth = auth();

// //? tables
db.admin = require(dbPath.admin)( sequelize , Sequelize );


module.exports = db;