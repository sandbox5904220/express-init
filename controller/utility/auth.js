require('dotenv').config();
const jwt = require('jsonwebtoken');
const statusCode = require('../../assets/statusCode');
const db = require('../db');
const { Op, sequelize, admin } = db;

const controller = {};

controller.login = async (req, res, next) => {
    console.log(req.body.userName, req.body.passWord)
    let query = await admin.findOne({ where: { userName: req.body.userName, passWord: req.body.passWord } });
    let result = JSON.parse(JSON.stringify(query));
    console.log(result)
    if (result) {
        let resVal = jwt.sign(
            result,
            process.env.TOKEN_KEY,
            { expiresIn: '10m' }
        );
        res.json({ status: statusCode.s200, value: resVal });
    } else {
        res.json({ status: statusCode.s4011, value: '' });
    }
};


controller.auth = async (req, res, next) => {
    try {
        if (!req.headers["authorization"]) return res.sendStatus(401);
        const token = req.headers["authorization"].replace("Bearer ", "");
    
        jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
          if (err) throw new Error(error)
          req.user = decoded
          delete req.user.exp
          delete req.user.iat
        })
        next()
      } catch (error) {
        return res.status(statusCode.s4031.code).json(statusCode.s4031);
      }
}

module.exports = controller;